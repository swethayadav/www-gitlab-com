---
layout: job_page
title: "Backend Developer, Gitaly"
---

Gitaly is a new service in our architecture that handles git and other filesystem
operations for GitLab instances, and aims to improve reliability and performance while
scaling to meet the needs of installations with thousands of concurrent users, including
our site GitLab.com. This position reports to the Gitaly Lead.


## Responsibilities

* Write performant, maintainable and elegant code and peer review others’ code.
* Ship small features independently.
* Be positive and solution oriented.
* Constantly improve the quality & security of the product.
* Take initiative in improving the software in small or large ways to address pain points in your own experience as a developer.
* Qualify developers for hiring.

## Within the Gitaly team specifically, in this role you will:

* Participate in architectural discussions and decisions surrounding Gitaly.
* Scope, estimate and describe tasks to reach the team’s goals.
* Collaborate on designing RPC interfaces for the Gitaly service
* Instrument, monitor and profile Gitaly in the production environment.
* Build dashboards and alerts to monitor the health of your services.
* Conduct acceptance testing of the features you’ve built.
* Educate all team members on best practices relating to high availability.


## Requirements

* Mandatory: production experience building, debugging, optimising software in large-scale, high-volume environments.
* Mandatory: Solid production Ruby experience.
* Highly desirable: Experience working with Go. It’s important that candidates must be willing to learn and work in both Go and Ruby.
* Highly desirable: experience with gRPC.
* Highly desirable: a good understanding of git’s internal data structures or experience running git servers. You can reason about software, algorithms, and performance from a high level.
* Understanding of how to build instrumented, observable software systems.
* Experience highly-available systems in production environments.
* Self-motivated and self-managing, with strong organizational skills.
* You share our values, and work in accordance with those values.
* A technical interview is part of the hiring process for this position.


## Workflow

The basics of GitLab development can be found in the [developer onboarding](/handbook/developer-onboarding/#basics-of-gitlab-development) document.

The handbook details the complete [GitLab Workflow](/handbook/communication/#gitlab-workflow).

## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)
- [Edge Team](/handbook/quality/edge)
- [Backstage speciality](/roles/specialist/backstage)
- [Issue Triage speciality](/roles/specialist/issue-triage)

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Candidates will be sent an assessment to complete,
* Once reviewed, selected candidates will be invited to schedule a 30min
  [screening call](/handbook/hiring/#screening-call) with our Global Recruiters.
* Selected candidates will be invited to schedule a 45min [technical interview](/handbook/hiring/technical)
  interview with a Gitaly Engineer.
* Candidates will then be invited for a 1 hour call with the Gitaly Lead.
* Candidates will be invited to schedule an interview with our VP of Engineering.
* Finally, candidates may be asked to have an interview with our CEO.
* Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
